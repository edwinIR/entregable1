package com.techu.entregable1.controlador;

import com.techu.entregable1.modelo.ModeloProducto;
import com.techu.entregable1.servicio.ServicioDatos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/api/v1/productos")
public class ControladorProducto {

    @Autowired
    private ServicioDatos servicioDatos;

    @GetMapping("/")
    public ResponseEntity obtenerProductos() {
        return ResponseEntity.ok(servicioDatos.obtenerProductos());
    }
    @GetMapping("/{id}")
    public ResponseEntity obtenerUnProducto(@PathVariable int id) {
        ModeloProducto pr = servicioDatos.obtenerProductoPorId(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    @PostMapping("/")
    public ResponseEntity crearProducto(@RequestBody ModeloProducto modeloProducto) {
        final ModeloProducto p= servicioDatos.agregarProducto(modeloProducto);
        return ResponseEntity.ok(p);
      //  return new ResponseEntity<>("Product created successfully!", HttpStatus.CREATED);
    }
    //IDENPOTENTE, CUANDO CADA OPERACION SIEMPRE DEVUELVE LOS MISMO
    @DeleteMapping("/{id}")
    public ResponseEntity borrarUnProducto(@PathVariable int id) {
        servicioDatos.borrarProducto(id);
        return new ResponseEntity<>("!Eliminado¡", HttpStatus.NO_CONTENT);
    }
    @PutMapping("/{id}")
    public ResponseEntity actualizarProducto(@PathVariable int id,@RequestBody ModeloProducto modeloProducto) {
        boolean p= servicioDatos. actualizarProducto(id,modeloProducto);
        if(p){
            return ResponseEntity.ok(p);
        }
        return new ResponseEntity<>("Producto no encontrado",HttpStatus.NOT_FOUND);
    }

}
