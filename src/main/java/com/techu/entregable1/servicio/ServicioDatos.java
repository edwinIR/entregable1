package com.techu.entregable1.servicio;

import com.techu.entregable1.modelo.ModeloProducto;
import com.techu.entregable1.modelo.ModeloUsuario;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class ServicioDatos {

    private final AtomicInteger secuenciaIdsProductos = new AtomicInteger(0);
    private final AtomicInteger secuenciaIdsUsuarios = new AtomicInteger(0);
    private final List<ModeloProducto> productos = new ArrayList<ModeloProducto>();

    //@PostMapping("/")
    public ModeloProducto agregarProducto( ModeloProducto producto){
        producto.setId(this.secuenciaIdsProductos.incrementAndGet());
        this.productos.add(producto);
        return producto;
    }
    public ModeloUsuario agregarUsuario(int idProducto,ModeloUsuario usuario){
        usuario.setId(this.secuenciaIdsUsuarios.incrementAndGet());
        this.obtenerProductoPorId(idProducto).getUsuarios().add(usuario);
        return usuario;
    }

    public List<ModeloProducto> obtenerProductos(){
            return Collections.unmodifiableList(this.productos);
    }
    public List<ModeloUsuario> obtenerUsuariosProducto(int idProducto){
        return Collections.unmodifiableList(this.obtenerProductoPorId(idProducto).getUsuarios());
    }

    public ModeloProducto obtenerProductoPorId(int id){
        for(ModeloProducto p : this.productos){
            if(id == p.getId() ){
                return p;
            }
        }
        return null;
    }
    public ModeloUsuario obtenerUsuarioPorId(int idProducto,int idUsuario){
        for(ModeloUsuario u : this.obtenerProductoPorId(idProducto).getUsuarios()){
            if(idUsuario == u.getId() ){
                return u;
            }
        }
        return null;
    }

    public boolean actualizarProducto( int id, ModeloProducto producto){
        /*for(ModeloProducto p : this.productos){
            if(id == p.getId() ){
                p.setMarca(producto.getMarca());
                p.setDescripcion(producto.getDescripcion());
                p.setPrecio(producto.getPrecio());
                p.setUsuarios(List.copyOf(producto.getUsuarios()));
                return true;
            }
        }*/
        for(int i=0;i<this.productos.size();i++){

            if(id == this.productos.get(i).getId() ){
                producto.setId(id);
                this.productos.set(i,producto);
                return true;
            }
        }
        return false;
    }
    public boolean actualizarUsuarioProducto( int idProducto,int idUsuario, ModeloUsuario usuario){
        List<ModeloUsuario> usuarios = this.obtenerProductoPorId(idProducto).getUsuarios();
        for(int i=0;i<usuarios.size();i++){
            if(idUsuario == usuarios.get(i).getId() ){
                usuario.setId(idUsuario);
                usuarios.set(i,usuario);
                return true;
            }
        }
        return false;
    }

    public boolean borrarProducto( int id){
        for(int i=0;i<this.productos.size();i++){
            if(id == this.productos.get(i).getId() ){
                this.productos.remove(i);
                return true;
            }
        }
        return false;
    }

    public boolean borrarUsuarioProducto( int idProducto,int idUsuario){
        List<ModeloUsuario> usuarios = this.obtenerProductoPorId(idProducto).getUsuarios();
        for(int i=0;i<usuarios.size();i++){
            if(idUsuario == usuarios.get(i).getId() ){
                usuarios.remove(i);
                return true;
            }
        }
        return false;
    }
}
